package com.example.joel.proyecto;

import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtUser = (TextView) findViewById(R.id.txtUser);
        final TextView txtPass = (TextView) findViewById(R.id.txtPass);
        Button btnAct2= (Button) findViewById(R.id.btnAct2);



        btnAct2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String User = txtUser.getText().toString();
                String Pass = txtPass.getText().toString();
                String title, msg;

                if(User != null && User.length()>0 && Pass != null && Pass.length()>0){

                    if(User.equals("batman") && Pass.equals("nanana")){
                        startActivity(new Intent(MainActivity.this,Activity2.class));
                    }else{
                        title ="Datos incorrectos";
                        msg=" por favor revisa el usuario y la contraseña";
                        getNotification(title,msg);
                    }
                }else{
                        title ="Campos vacios ";
                        msg=" falta la informacion del usuario o contraseña";
                        getNotification(title,msg);
                }



            }
        });

    }

    public void getNotification(String title, String msg){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.error);
        //builder.setContentIntent();
        builder.setAutoCancel(true);
        builder.setContentTitle(title);
        builder.setContentText(msg);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(1,builder.build());


    }

}
