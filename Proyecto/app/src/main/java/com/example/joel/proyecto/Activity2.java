    package com.example.joel.proyecto;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.VideoView;

import java.util.List;

    public class Activity2 extends AppCompatActivity {


        VideoView video;
        Spinner spinner;
        Button btnVer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        video = (VideoView) findViewById(R.id.video);
        spinner = (Spinner) findViewById(R.id.ltsPelis);
        btnVer = (Button) findViewById(R.id.btnPlay);



        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.pelis, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);


        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = spinner.getSelectedItem().toString();
                Uri link = Uri.parse(url);
                video.setVideoURI(link);
                video.start();
            }
        });







    }
}
